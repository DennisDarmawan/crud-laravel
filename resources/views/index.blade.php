@extends('adminlte.master')

@section('content')

<div class="mt-3 ml-3 mr-3">
  <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data Pertanyaan</h3>

        <div class="card-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Body</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($pertanyaan as $key => $pertanyaan)
            <tr>
              <td> {{ $key + 1}} </td>
              <td> {{ $pertanyaan->title }} </td>
              <td> {{ $pertanyaan->body }} </td>
              <td> actions </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>

@endsection