<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class pertanyaanController extends Controller
{
    public function create(){
        return view('create');
    }

    public function store(Request $request){
        // dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "judul"=>$request["judul"],
            "isi"=>$request["isi"]
        ]);

        return redirect('/pertanyaan');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('index', compact('pertanyaan'));
    }
}
